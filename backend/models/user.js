//dependecies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const userSchema = new Schema({
    username: {
        type: String,
		required: true,
		trim: true 
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    number: {
        type: String,
        required: true
    },
    email: {
        type: String,
		required: true,
		unique: true,
		minLength: 5
    },
    password: {
        type: String,
		required: true,
		minLength: 8
    },
    isAdmin: {
        type: Boolean,
		default: false
    },
    createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model("User", userSchema);