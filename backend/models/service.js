//dependecies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const serviceSchema = new Schema({
	name: {
        type: String,
        required: true
    },
    duration: {
        type: String,
        default: "Edit duration of service.",
        required: true
    },
    description: {
        type: String,
        default: "Edit service description.",
        required: true
    },
    price: {
        type: String,
        required: true
    },
    categoryId: {
        type: String,
        required: true
    },
    createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model("Service", serviceSchema);