//dependecies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const appointmentSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    categoryId: {
        type: String,
        required: true
    },
    serviceId: {
        type: String,
        required: true
    },
    bookingDate: {
        type: String,
        default: Date.now
    },
    timeslotId: {
        type: String,
        required: true
    },
    isCompleted: {
        type: Boolean,
        default: false
    },
    dateCompleted: {
        type: String,
        default: "Ongoing"
    },
    createdAt: {
        type: String,
        default: Date.now
    },
	updatedAt: {
		type: String,
		default: "Pending"
	}
})

//export
module.exports = mongoose.model("Appointment", appointmentSchema);