//dependecies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const timeslotSchema = new Schema({
	time: {
		type: String,
		required: true
	},
	createdAt: {
		type: String,
		default: Date.now
	},
	updatedAt: {
		type: String,
		default: "Pending"
	}
});

//export schema as model
module.exports = mongoose.model("Timeslot", timeslotSchema);