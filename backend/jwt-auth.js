const jwt = require("jsonwebtoken");
const secret = "thelazybabe";

module.exports.createToken = (user) => {
	let data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
		username: user.username,
		firstName: user.firstName
	}
	return jwt.sign(data, secret, { expiresIn: '2h' })
}