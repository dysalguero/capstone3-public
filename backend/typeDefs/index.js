const { gql } = require("apollo-server-express");

const typeDefs = gql`
    type Appointment {
        id: ID
        userId: String
        categoryId: String
        serviceId: String
        bookingDate: String
        timeslotId: String
        isCompleted: Boolean
        dateCompleted: String
        createdAt: String
        updatedAt: String
        user: User
        category: Category
        service: Service
        timeslot: Timeslot
    }

    type Category {
        id: ID
        name: String
        createdAt: String
        updatedAt: String
        services: [Service]
        appointments: [Appointment]
    }

    type Service {
        id: ID
        name: String
        duration: String
        description: String
        price: String
        categoryId: String
        createdAt: String
        updatedAt: String
        category: Category
        appointments: [Appointment]
    }

    type Timeslot {
        id: ID
        time: String
        createdAt: String
        updatedAt: String
        appointments: [Appointment]
    }

    type User {
        id: ID
        username: String
        firstName: String
        lastName: String
        number: String
        email: String
        password: String
        isAdmin: Boolean
        createdAt: String
        updatedAt: String
        token: String
        appointments: [Appointment]
    }

    type Query {
        appointments: [Appointment!]
        categories: [Category!]
        services: [Service!]
        servicesByCategory(id: ID!): [Service!]
        servicesByCategoryName(name: String!): [Service!]
        timeslots: [Timeslot!]
        users: [User!]
        appointment(id: ID!): Appointment
        category(id: ID!): Category
        service(id: ID!): Service
        timeslot(id: ID!): Timeslot
        user(id: ID!): User
    }

    type Mutation {
        storeAppointment(
            userId: String!
            categoryId: String!
            serviceId: String!
            bookingDate: String!
            timeslotId: String!
        ): Appointment

        storeCategory(
            name: String!
        ): Category

        storeService(
            name: String!
            duration: String!
            price: String!
            description: String!
            categoryId: String!
        ): Service

        storeTimeslot(
            time: String!
        ): Timeslot

        registerUser(
            username: String!
            firstName: String!
            lastName: String!
            number: String!
            email: String!
            password: String!
        ): Boolean

        destroyAppointment(
            id: ID!
        ): Appointment

        destroyCategory(
            id: ID!
        ): Category

        destroyService(
            id: ID!
        ): Service

        destroyTimeslot(
            id: ID!
        ): Timeslot

        destroyUser(
            id: ID!
        ): User

        updateAppointment(
            id: ID!
            userId: String!
            categoryId: String!
            serviceId: String!
            bookingDate: String!
            timeslotId: String!
        ): Appointment

        updateCategory(
            id: ID!
            name: String!
        ): Category

        updateService(
            id: ID!
            name: String!
            duration: String!
            price: String!
            description: String!
            categoryId: String!
        ): Service

        updateTimeslot(
            id: ID!
            time: String!
        ): Timeslot

        updateUser(
            id: ID!
            username: String!
            firstName: String!
            lastName: String!
            number: String!
            email: String!
            password: String!
        ): User

        loginUser(
			email: String
			password: String
		): User
    }
`;

module.exports = typeDefs;