const express = require("express");
const app = express();
const { ApolloServer } = require("apollo-server-express");
const mongoose = require("mongoose");
const cors = require("cors");

//file imports
const typeDefs = require("./typeDefs");
const resolvers = require("./resolvers");

//apollo server
const server = new ApolloServer({
	typeDefs,
	resolvers,
	playground: true,
	introspection: true
})

//db connection
mongoose.connect("mongodb://localhost:27017/thelazybabe", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
})
mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB server");
})

// use middleware
server.applyMiddleware({
	app,
	path: "/graphql"
})
app.use(cors());

//initialize server
app.listen(4000, ()=>{
	console.log("No issues...");
})

// http://localhost:4000/graphql