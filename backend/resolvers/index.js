const Appointment = require("../models/appointment");
const Category = require("../models/category");
const Service = require("../models/service");
const Timeslot = require("../models/timeslot");
const User = require("../models/user");

//dependency
const bcrypt = require("bcrypt");
const auth = require("../jwt-auth");

const resolvers = {
    Appointment: {
        user: ({ userId }, args) => {
            return User.findById(userId);
        },
        category: ({ categoryId }, args) => {
            return Category.findById(categoryId);
        },
        service: ({ serviceId }, args) => {
            return Service.findById(serviceId);
        },
        timeslot: ({ timeslotId }, args) => {
            return Timeslot.findById(timeslotId);
        }
    },

    Category: {
        appointments: ({ _id }, args) => {
            return Appointment.find({categoryId: _id});
        },
        services: ({ _id }, args) => {
            return Service.find({categoryId: _id});
        }
    },

    Service: {
        category: ({ categoryId }, args) => {
            return Category.findById(categoryId);
        },
        appointments: ({ _id }, args) => {
            return Appointment.find({serviceId: _id});
        }
    },

    Timeslot: {
        appointments: ({ _id }, args) => {
            return Appointment.find({timeslotId: _id});
        }
    },

    User: {
        appointments: ({ _id }, args) => {
            return Appointment.find({userId: _id});
        }
    },

    Query: {
        appointments: () => {
            return Appointment.find({});
        },
        categories: () => {
            return Category.find({});
        },
        services: () => {
            return Service.find({});
        },
        servicesByCategory: (parent, {id}) => {
            return Service.find({categoryId: id});
        },
        servicesByCategoryName: (parent, { name }) => {
            let query = Category.findOne({ name });

            return query.then(category => {
                return Service.find({ categoryId: category._id})
            })
        },
        timeslots: () => {
            return Timeslot.find({});
        },
        users: () => {
            return User.find({});
        },
        appointment: (parent, {id}) => {
            return Appointment.findById(id);
        },
        category: (parent, {id}) => {
            return Category.findById(id);
        },
        service: (parent, {id}) => {
            return Service.findById(id);
        },
        timeslot: (parent, {id}) => {
            return Timeslot.findById(id);
        },
        user: (parent, {id}) => {
            return User.findById(id);
        }
    },

    Mutation: {
        storeAppointment: (parent, { userId, categoryId, serviceId, bookingDate, timeslotId }) => {
            
            let appointment = new Appointment({ userId, categoryId, serviceId, bookingDate, timeslotId })
            return appointment.save();
        },
        storeCategory: (parent, { name }) => {

            let category = new Category({ name })
            return category.save();
        },
        storeService: (parent, { name, duration, description, price, categoryId }) => {

            let service = new Service({ name, duration, description, price, categoryId })
            return service.save();
        },
        storeTimeslot: (parent, { time }) => {

            let timeslot = new Timeslot({ time })
            return timeslot.save();
        },
        registerUser: (parent, { username, firstName, lastName, number, email, password }) => {

            let user = new User({ 
                username, 
                firstName, 
                lastName, 
                number, 
                email, 
                password : bcrypt.hashSync(password, 8) 
            }) 
            return user.save().then((user, err) => {
                return err ? false : true;
            });
        },
        destroyAppointment: (parent, {id}) => {
            return Appointment.findByIdAndRemove(id)
        },
        destroyCategory: (parent, {id}) => {
            return Category.findByIdAndRemove(id)
        },
        destroyService: (parent, {id}) => {
            return Service.findByIdAndRemove(id)
        },
        destroyTimeslot: (parent, {id}) => {
            return Timeslot.findByIdAndRemove(id)
        },
        destroyUser: (parent, {id}) => {
            return User.findByIdAndRemove(id)
        },
        updateAppointment: (parent, {id, userId, categoryId, serviceId, bookingDate, timeslotId }) => {
            return Appointment.findByIdAndUpdate(id, { userId, categoryId, serviceId, bookingDate, timeslotId }, { new: true })
        },
        updateCategory: (parent, {id, name}) => {
            return Category.findByIdAndUpdate(id, { name }, { new: true })
        },
        updateService: (parent, {id, name, duration, description, price, categoryId}) => {
            return Service.findByIdAndUpdate(id, {name, duration, description, price, categoryId}, { new: true })
        },
        updateTimeslot: (parent, {id, time}) => {
            return Timeslot.findByIdAndUpdate(id, {time}, {new: true})
        },
        updateUser: (parent, {id, username, firstName, lastName, number, email, password}) => {
            return User.findByIdAndUpdate(id, {username, firstName, lastName, number, email, password}, { new: true })
        },
        loginUser: (parent, { email, password }) => {
            let query = User.findOne({ email });
            
            return query.then(user => {
                if(user === null) {
                    return null;
                }
                let isPasswordMatched = bcrypt.compareSync( password, user.password);

                // console.log(isPasswordMatched);

				//create login token
				if(isPasswordMatched){
					user.token = auth.createToken(user.toObject());
					return user;
				} else {
					return null;
				}
            })
        }
    }
}

module.exports = resolvers;
