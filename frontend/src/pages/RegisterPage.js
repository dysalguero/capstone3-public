import React, {Fragment} from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Container,
	Columns
} from 'react-bulma-components';

import RegisterForm from '../components/forms/RegisterForm';

const RegisterPage = props => {
	return (
		<Fragment>
			<div className="sectionTitleWrapper has-text-centered">
				<Container className="sectionTitleHolder">
					<Heading className="fontQuentin textCapitalize size70px">Register</Heading>
				</Container>
			</div>
			<Section>
				<div>
					<Columns>
						<Columns.Column></Columns.Column>
						<Columns.Column className="is-8 my-30">
							<RegisterForm />
						</Columns.Column>
						<Columns.Column></Columns.Column>
					</Columns>
				</div>
			</Section>
		</Fragment>
	)
}

export default RegisterPage;