import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Hero,
    Container,
    Section,
    Columns,
    Table,
    Card
} from 'react-bulma-components';

import ReactWOW from 'react-wow';

const HomePage = props => {
    // console.log(props.currentUser().isAdmin)

    let component = "";

    if(props.currentUser().isAdmin) {
        component = (
            <Fragment>
                <Hero className="hero hero-home2 is-dark is-fullheight">
                    <Hero.Body>
                        <Container className="container has-text-centered">
                            <div className="size60px fontQuentin textCapitalized my-30 is-gold">Dashboard</div>
                                <Columns> 
                                    <Columns.Column>
                                        <div class="card">
                                            <header class="card-header">
                                                <p class="card-header-title">
                                                Appointments
                                                </p>
                                                <a href="/appointments" class="card-header-icon" aria-label="more options">
                                                    <span class="icon">
                                                        <i class="fas fa-angle-down" aria-hidden="true"></i>
                                                    </span>
                                                </a>
                                            </header>
                                            <div class="card-content">
                                                <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
                                                <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                                                <br/>
                                                <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                                </div>
                                            </div>
                                            {/* <footer class="card-footer">
                                                <a href="#" class="card-footer-item">Save</a>
                                                <a href="#" class="card-footer-item">Edit</a>
                                                <a href="#" class="card-footer-item">Delete</a>
                                            </footer> */}
                                        </div>
                                    </Columns.Column>
                                    <Columns.Column>
                                        <div class="card">
                                            <header class="card-header">
                                                <p class="card-header-title">
                                                Users
                                                </p>
                                                <a href="/users" class="card-header-icon" aria-label="more options">
                                                <span class="icon">
                                                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                                                </span>
                                                </a>
                                            </header>
                                            <div class="card-content">
                                                <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
                                                <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                                                <br/>
                                                <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                                </div>
                                            </div>
                                            {/* <footer class="card-footer">
                                                <span class="card-footer-item"> <em>Current Users</em></span>
                                            </footer> */}
                                        </div>
                                    </Columns.Column>
                                </Columns>
                                <Columns>
                                <Columns.Column>
                                        <div class="card">
                                            <header class="card-header">
                                                <p class="card-header-title">
                                                Categories
                                                </p>
                                                <a href="/categories" class="card-header-icon" aria-label="more options">
                                                <span class="icon">
                                                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                                                </span>
                                                </a>
                                            </header>
                                            <div class="card-content">
                                                <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
                                                <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                                                <br/>
                                                <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                                </div>
                                            </div>
                                            <footer class="card-footer">
                                                <span class="card-footer-item is-pulled-left"> View details</span>
                                            </footer>
                                        </div>
                                    </Columns.Column>
                                    <Columns.Column>
                                        <div class="card">
                                            <header class="card-header">
                                                <p class="card-header-title">
                                                Services
                                                </p>
                                                <a href="/services" class="card-header-icon" aria-label="more options">
                                                <span class="icon">
                                                    <i class="fas fa-angle-down" aria-hidden="true"></i>
                                                </span>
                                                </a>
                                            </header>
                                            <div class="card-content">
                                                <div class="content">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
                                                <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                                                <br/>
                                                <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                                                </div>
                                            </div>
                                            {/* <footer class="card-footer">
                                                <a href="#" class="card-footer-item">Save</a>
                                                <a href="#" class="card-footer-item">Edit</a>
                                                <a href="#" class="card-footer-item">Delete</a>
                                            </footer> */}
                                        </div>
                                    </Columns.Column>
                                </Columns>
                        </Container>
                    </Hero.Body>
                </Hero>
            </Fragment>
        )
    } else {
        component = (
            <Fragment>     
            <Hero className="hero hero-home is-dark is-fullheight">
                <Hero.Body>
                    <Container className="container has-text-centered">
                        <div className="size60px fontQuentin textCapitalized my-18 wow pulse">Get Pampered</div>
                        <h2 className="size12px spacing25rem my-18">
                            Philippines • Since 2019
                        </h2>
                        <img src="../images/divider.png" className="my-18"/>
                    </Container>
                </Hero.Body>
            </Hero>

            {/* SECTION ABOUT */}
            <Section className="my-100">
                <div className="backText is-hidden-touch has-text-centered">
                    <div className="fontQuentin textCapitalized">The Lazy Babe</div>
                </div>
                <Container className="my-30 has-text-centered">
                    <h1 className="size70px">Our Story</h1>
                    <Columns className="is-centered my-30">
                        <Columns.Column className="is-half">
                            <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius dicta vero ipsa quae magni, maiores harum modi perspiciatis rerum nostrum. Quaerat placeat rerum voluptatibus aperiam facere assumenda unde doloribus laboriosam!
                            </p>
                            <Link className="button is-link my-30" to="/about">
                                Read More
                            </Link>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>

            {/* SECTION TREAT */}
            <Section className="sectionTitleWrapperDark2 has-text-centered">
                <Container className="my-100">
                    <Columns>
                        <Columns.Column className="my-30">
                            <img src="../images/icon1.png" alt="icon" />
                            <br/><br/>
                            <h1 className="size20px spacing5px my-5">Treat</h1>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ad laboriosam deserunt.
                            </p>
                        </Columns.Column>
                        <Columns.Column className="my-30">
                            <img src="../images/icon2.png" alt="icon" />
                            <br/><br/>
                            <h1 className="size20px spacing5px my-5">Pamper</h1>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ad laboriosam deserunt.
                            </p>
                        </Columns.Column>
                        <Columns.Column className="my-30">
                            <img src="../images/icon3.png" alt="icon" />
                            <br/><br/>
                            <h1 className="size20px spacing5px my-5">Rejuvenate</h1>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis ad laboriosam deserunt.
                            </p>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>

            {/* SECTION WORKING HOURS */}
            <Section>
                <Container className="my-100">
                    <Columns>
                        <Columns.Column className="my-30 has-text-centered">
                        <div className="backTextHours is-hidden-touch has-text-centered">
                            <div className="fontQuentin textCapitalized">Visit us</div>
                        </div>
                        <h1 className="size50px">Working Hours</h1>
                        
                            <Table className="table  my-30">
                                <thead>
                                    <tr>
                                        <th>Mondays - Fridays</th>
                                        <td>11AM - 8PM</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Saturdays</th>
                                        <td>11AM - 9PM</td>
                                    </tr>
                                    <tr>
                                        <th>Sundays</th>
                                        <td>Closed</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Columns.Column>
                        <Columns.Column className="verticalAlign">
                            <div className="has-text-centered">
                                <img src="../images/h2-img-1.jpg" width="70%"/>
                            </div>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>

            {/* TESTIMONALS SECTION */}
            <Section className="sectionHomeBg1">
                <Container className="my-30 has-text-centered">
                    <div className="backTextTesti is-hidden-touch has-text-centered">
                        <div className="fontQuentin textCapitalized">Clients said...</div>
                    </div>
                    <Columns className="is-centered my-30">
                        <Columns.Column className="is-half">
                            <div className="size60px is-uppercase">Reviews</div>
                            <p className=" my-30">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius dicta vero ipsa quae magni, maiores harum modi perspiciatis rerum nostrum. Quaerat placeat rerum voluptatibus aperiam facere assumenda unde doloribus laboriosam!
                            </p>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>
        </Fragment>      
        )
        
    }

	return (
        <div>
        { component }
        </div>
        
	)
}

export default HomePage;