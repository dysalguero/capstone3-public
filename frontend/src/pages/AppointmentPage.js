import React, {Fragment} from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Container,
} from 'react-bulma-components';
import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';


//FORM
import AddAppointmentForm from '../components/forms/AddAppointmentForm';

//QUERIES
import {
	getCategoriesQuery,
	getAppointmentsQuery,
	getTimeslotQuery
} from '../graphql/queries';

const AppointmentPage = props => {

	// console.log(props.getTimeslotQuery)
	// console.log(props.getCategoriesQuery.categories)

	// console.log(props.currentUser());
	const timeslots = props.getTimeslotQuery;

	const addAppointment = (newAppointment) => {
		props.storeAppointmentMutation({
			variables: newAppointment,
			refetchQueries: [{ query: getAppointmentsQuery }]
		})
	}
	

	return (
		<Fragment>
			{/* VIEW FOR USER */}
			<div className="sectionTitleWrapper has-text-centered">
				<Container className="sectionTitleHolder">
					<Heading className="fontQuentin textCapitalize size30px">Appointment Booking</Heading>
					<h1 className="size30px">Schedule your next visit</h1>
				</Container>
			</div>
			<Section>
				<Container className="my-30">
					<AddAppointmentForm addAppointment={addAppointment} timeslots={timeslots.timeslots}/>
				</Container>
				<Container className="my-30">
					<h1 className="my-30">Current Bookings:</h1>
					<h4>Show all appointments here.</h4>
				</Container>
			</Section>	
		</Fragment>
	)
}

export default compose(
	graphql(getCategoriesQuery, { name: 'getCategoriesQuery' }),
	graphql(getAppointmentsQuery, { name: 'getAppointmentsQuery' }),
	graphql(getTimeslotQuery, { name: 'getTimeslotQuery' }),

	)(AppointmentPage);