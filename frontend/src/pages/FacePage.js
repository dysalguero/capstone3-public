import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Container
} from 'react-bulma-components';
import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';

//QUERIES
import { 
	getservicesByCategory
} from '../graphql/queries';

const FacePage = props => {

	// console.log(props.getservicesByCategory);
    return (
		<Fragment>
			<div className="sectionTitleWrapper headerBG">
				<Container className="sectionTitleHolder">
					<Heading className="fontQuentin textCapitalize size50px">Face</Heading>
					{/* BREADCRUMBS */}
					<nav className="breadcrumb is-small" aria-label="breadcrumbs">
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="/services">Services</a></li>
							<li className="is-active"><a href="#" aria-current="page">Face</a></li>
						</ul>
					</nav>
				</Container>
			</div>

			<Section>
				<h1>Test</h1>
			</Section>
		</Fragment>
		
	)
}

export default compose(
	graphql(getservicesByCategory, { 
			options: props => {
				return {
					variables: {
						id: props.match.params.id
					}
				}
			},
			name: 'getservicesByCategory' 
	}),
)(FacePage);