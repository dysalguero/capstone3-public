import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns,
	Container
} from 'react-bulma-components';
import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';
import Swal from 'sweetalert2';

//COMPONENTS
import AddCategoryForm from '../components/forms/AddCategoryForm';
import CategoriesList from '../components/lists/CategoriesList';


//QUERIES
import { 
	getCategoriesQuery
} from '../graphql/queries';

//MUTATIONS
import {
    StoreCategoryMutation,
    destroyCategoryMutation,
    updateCategoryMutation
} from '../graphql/mutations';

const CategoriesPage = props => {
    const categories = props.getCategoriesQuery.categories;
    // console.log(categories);

    //STORE
    const addCategory = (newCategory) => {
		props.StoreCategoryMutation({
			variables: newCategory,
			refetchQueries: [{ query: getCategoriesQuery }]
		})
    }

    // DESTROY
	const destroyCategory = (id, name) => {
		Swal.fire({
			title: "Delete Category",
			text: `Do you want to remove ${name}?`,
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Remove",
			confirmButtonColor: "#ff3860",
			reverseButtons: true
		}).then((formData) => {
			// console.log(formData)
			if(formData.value){
				// console.log("delete");
				props.destroyCategoryMutation({
					variables: { id: id },
					refetchQueries: [{ query: getCategoriesQuery }]
				})
				.then(response =>{
					// console.log(response);
					let result = response.data.destroyCategory;

					if(result){
						Swal.fire({
							title: "Category Deleted",
							text: `${name} has been deleted.`,
							icon: "success"
						})
					}
				})
			}
		})
    }
    
    //UPDATE
    const updateCategory = (id, name) => {
        Swal.fire({
			title: "Update Category",
			icon: "info",
			html:
				`<div class="swal2-content2">Name</div><input id="name" class="swal2-input swal2-input2" value="${name}">`,
			showCancelButton: true,
			confirmButtonText: "Update",
			confirmButtonColor: "#ffdd57",
			reverseButtons: true,
			allowEnterKey: true,
			preConfirm: () => {
				let newName = document.querySelector("#name").value;
				// console.log(newName);

				if(newName === "") {
					Swal.showValidationMessage("Please fill out all fields.");
				} else {
					return {
						id: id,
						name: newName
					}
				}
			}			
        })
        .then(formData => {
			// console.log(formData);
			if(formData.value) {
				props.updateCategoryMutation({
					variables: formData.value,
					refetchQueries: [{ query: getCategoriesQuery }]
				})

				.then(response => {
					let result = response.data.updateCategory;
					// console.log(result);
					if(result) {
						Swal.fire({
							title: "Category Updated",
							text: `Category has been updated.`,
							icon: "success",
							timer: 3000
						})
					}
				})
			}
		})
    }

    return (
        <Fragment>
            <div className="sectionTitleWrapper">
				<Container className="sectionTitleHolder">
					<Heading className="fontQuentin textCapitalize size50px">Categories</Heading>
				</Container>
			</div>
            <Section size="medium">
                <Container>
                    <Columns>
                        <Columns.Column size="is-6">
                            <AddCategoryForm addCategory={addCategory} />
                        </Columns.Column>
                        <Columns.Column size="is-6">
                            <CategoriesList 
                                categories={categories}
                                destroyCategory={destroyCategory}
                                updateCategory={updateCategory}
                            />
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>
        </Fragment>
    )
}

export default compose(
	graphql(getCategoriesQuery, { name: 'getCategoriesQuery' }),
    graphql(StoreCategoryMutation, { name: 'StoreCategoryMutation' }),
    graphql(destroyCategoryMutation, { name: 'destroyCategoryMutation' }),
    graphql(updateCategoryMutation, { name: 'updateCategoryMutation' })
	)(CategoriesPage);

