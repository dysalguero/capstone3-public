import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns,
	Container
} from 'react-bulma-components';

const AboutPage = props => {
    return (
        <Fragment>
            <div className="sectionTitleWrapper headerBGAbout">
                <Container className="sectionTitleHolder">
                    <Heading className="fontQuentin textCapitalize size50px is-white">About Us</Heading>
                </Container>
            </div>   

            {/* HISTORY SECTION */}
            <Section>
                <Container className="my-100">   
                    <Columns className="is-half">
                        <Columns.Column size="is-5">
                            <div className="backTextAbout is-hidden-touch">
                                <div className="fontQuentin textCapitalized">History</div>
                            </div>
                            <h1 className="size50px is-pulled-right">Our Story</h1>
                            <br />
                            <br />
                            <br />
                        </Columns.Column>
                        <Columns.Column size="is-5">
                            <h1 className="size20px my-18">MEET DR. STACY SMITH</h1>
                            <p>
                                <b><em>Dr. Stacy Smith </em></b> - a celebrated dermatologist and doctor to the stars is known for pioneering the skincare, laser, and liposuction industry in the Philippines. After gaining her degree in Medicine and Surgery at the University of Santo Tomas and a Diploma in Dermatology from the Institute of Dermatology in Bangkok, she opened her very first clinic at the Medical Towers in Makati in 1990. Today, she is the President and Medical Director of the country’s most trusted beauty empire, the Belo Medical Group, which currently operates 12 clinics in Metro Manila, one in Cebu, and one in Davao.
                            </p>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>
                   
            {/* SUBSCRIBE MAILING SECTION */}
            <Section className="sectionTitleSubscribe is-centered">
                <Container className="has-text-centered my-100 ">   
                    <Columns className="is-half is-centered">
                        <Columns.Column size="7">
                            <h5 className="spacing25rem is-bold my-18">
                                <b>WANT GREAT DEALS?</b>
                            </h5>
                            <h1 className="size30px is-bold  my-18">Get on the list for exclusive coupons, bundle Promos and more...</h1>
                            <div class="field has-addons my-18">
                                <p class="control is-expanded">
                                    <input className="input" type="text" placeholder="johnsmith@example.com" />
                                </p>
                                <p class="control">
                                    <a className="button is-primary" href="#">
                                        I'M IN!
                                    </a>
                                </p>
                            </div>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>

            {/* ABOUT ME 2 SECTION */}
            <Section className="sectionTitle2ndAbout ">
                <Container className="my-100">   
                    <Columns className="is-half has-text-centered">
                        <Columns.Column size="5">
                            <h5 className="spacing25rem size12px">PHILIPPINES • SINCE 2019</h5>
                            <div className="backTextAbout2 is-hidden-touch">
                                <div className="fontQuentin textCapitalized">Story</div>
                            </div>
                            <h1 className="size50px">About Our Story</h1>
                            <p className="my-18">
                                We are a very personal clinic. When I put up the brand in 1990, I just wanted to help as many people as I could, people who shared the same insecurities as I did. I wanted to make it my life’s resolution to make everyone beautiful.
                            </p>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>
        </Fragment>
    )
}

export default AboutPage;