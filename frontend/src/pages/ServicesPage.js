import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns,
	Container
} from 'react-bulma-components';
import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';
import Swal from 'sweetalert2';

//QUERIES
import { 
	getCategoriesQuery,
	getServicesQuery
} from '../graphql/queries';

//MUTATIONS
import {
	storeServiceMutation,
	destroyServiceMutation,
	updateServiceMutation
} from '../graphql/mutations';

//COMPONENTS
import FaceList from '../components/lists/FaceList';
import LashesList from '../components/lists/LashesList';
import NailsList from '../components/lists/NailsList';
import BrowsList from '../components/lists/BrowsList';
import AddServicesForm from '../components/forms/AddServicesForm';
import ServicesList from '../components/lists/ServicesList';

const ServicesPage = props => {
	const services = props.getServicesQuery;
	const categories = props.getCategoriesQuery.categories;

	//function populateCategoryOption
    const populateCategoryOption = () => {
        let options = {};
        props.getCategoriesQuery.categories.map(category => {
			return (options[category.id] = category.name);
		})
		return options;
    }
	
	// ADD SERVICE
	const addService = (newService) => {
		props.storeServiceMutation({
			variables: newService,
			refetchQueries: [{ query: getServicesQuery }]
		})
	}

	// DESTROY SERVICE
	const destroyService = (id, name) => {
		Swal.fire({
			title: "Delete Service",
			text: `Do you want to remove ${name}?`,
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Remove",
			confirmButtonColor: "#ff3860",
			reverseButtons: true
		}).then((formData) => {
			// console.log(formData)
			if(formData.value){
				// console.log("delete");
				props.destroyServiceMutation({
					variables: { id: id },
					refetchQueries: [{ query: getServicesQuery }]
				})
				.then(response =>{
					// console.log(response);
					let result = response.data.destroyService;

					if(result){
						Swal.fire({
							title: "Service Deleted",
							text: `${name} has been deleted.`,
							icon: "success"
						})
					}
				})
			}
		})
	}

	// UPDATE SERVICE
	const updateService = (id, name, duration, price, description, categoryId, categoryName) => {
		Swal.fire({
			title: "Update Service",
			icon: "info",
			html:
				`<div class="swal2-content2">Name</div><input id="name" class=" swal2-input swal2-input2" value="${name}">` +
				`<div class="swal2-content2">Duration</div><input id="duration" class="swal2-input swal2-input2" value="${duration}">` +
				`<div class="swal2-content2">Price</div><input id="price" class="swal2-input swal2-input2" value="${price}">` +
				`<div class="swal2-content2">Description</div><input id="description" class="swal2-input swal2-input2" value="${description}">` +
				`<div class="swal2-content2">Category</div>`,
			input: 'select',
			inputOptions: populateCategoryOption(),
			inputValue: categoryId,
			inputPlaceholder: 'Select a category...',
			showCancelButton: true,
			confirmButtonText: "Update",
			confirmButtonColor: "#ffdd57",
			reverseButtons: true,
			inputAutoTrim: true,
			allowEnterKey: true,
			preConfirm: () => {
				let newName = document.querySelector("#name").value;
				let newDuration = document.querySelector("#duration").value;
				let newPrice = document.querySelector("#price").value;
				let newDescription = document.querySelector("#description").value;
				let newCategoryId = document.querySelector(".swal2-select").value;

				// console.log(id)

				//VALIDATION
				if(newName === "" || newDuration === "" || newPrice === "" || newDescription === "") {
					Swal.showValidationMessage("Please fill out all fields.");
				} else {
					return {
						id: id,
						name: newName,
						duration: newDuration,
						price: newPrice,
						description: newDescription,
						categoryId: newCategoryId
					}
				}
			}
		})
		.then(formData => {
			// console.log(formData.value);
			// console.log(props.updateServiceMutation);
			if(formData.value) {
				props.updateServiceMutation({
					variables: formData.value,
					refetchQueries: [{ query: getServicesQuery }]
				})
				.then(response => {
					// console.log(response);
					let result = response.data.updateService;
					if(result) {
						Swal.fire({
							title: "Service Updated",
							text: `${name} has been updated.`,
							icon: "success",
							timer: 2000
						})
					}
				})
				.catch(error => {
					console.log(error)
				})
			}
		}).catch(err => {
			console.log(err)
		})

	}

	// console.log(props.currentUser().isAdmin)

	let component = "";

    if(props.currentUser().isAdmin) {
		component = (
            <Fragment>
				<div className="sectionTitleWrapper">
					<Container className="sectionTitleHolder">
						<Heading className="fontQuentin textCapitalize size50px">Services</Heading>
					</Container>
				</div>
				<Section size="medium">
					<Container>
						<Columns>
							<Columns.Column className="is-4">
								<AddServicesForm addService={addService}/>
							</Columns.Column>
							<Columns.Column className="is-8">
								<ServicesList 
									services={services.services}
									destroyService={destroyService}
									updateService={updateService}
								/>
							</Columns.Column>
						</Columns>
					</Container>
				</Section>
            </Fragment>
        )

    } else {
        component = (
			<Fragment>
				{/* SECTIONS FACE */}
				<div className="sectionTitleWrapperDark3 headerBGServices">
					<Container className="sectionTitleHolder">
						<Heading className="fontQuentin textCapitalize size50px is-white">Our Services</Heading>
					</Container>
				</div>
				<Section>
					<FaceList services={services.services} categories={categories} categoryName="Face" />
				</Section>

				{/* SECTIONS LASHES AND NAILS */}
				<Section className="sectionTitleSubscribe">
					<Container>
						<Columns>
							{/* SECTIONS NAILS */}
							<NailsList services={services.services} categories={categories} categoryName="Nails"/>
							{/* SECTIONS LASHES */}
							<LashesList services={services.services} categories={categories} categoryName="Lashes"/>
						</Columns>
					</Container>	
				</Section>

				{/* SECTIONS BROWS */}
				<Section>
					<Container>
						<Columns >
							<BrowsList services={services.services} categories={categories} categoryName="Brows"/>
						</Columns>
					</Container>	
				</Section>			
            </Fragment>
        )
	}
	
	return (
		<div>
			
			{component}
		</div>
	)
}

export default compose(
	graphql(getCategoriesQuery, { name: 'getCategoriesQuery' }),
	graphql(getServicesQuery, { name: 'getServicesQuery' }),
	graphql(storeServiceMutation, { name: 'storeServiceMutation' }),
	graphql(destroyServiceMutation, { name: 'destroyServiceMutation' }),
	graphql(updateServiceMutation, { name: 'updateServiceMutation' }),
	)(ServicesPage);