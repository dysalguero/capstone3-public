import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Container,
	Hero
} from 'react-bulma-components';
import { Link } from 'react-router-dom';

const NotFoundPage = props => {
	return (
		<Hero className="hero is-fullheight notFoundPageBG is-black">
            <Hero.Body>
				<Container>
					<Section size='medium' className="has-text-centered is-half">
						{/* <img src="../images/divider.png" alt="divider" /> */}
						<div className="fontQuentin size70px">Page Not Found</div>
						<br />
						<div className="columns is-centered">
							<div className="column is-4">
								Oops! The page you are looking for does not exist. It might have been moved or deleted.
							</div>
						</div>
						<div className="columns is-centered">
							<div className="column is-4">
								<Link className="button cssbutton is-outlined is-uppercase" to="/">Return to Home</Link>
							</div>
						</div>
					</Section>
				</Container>
			</Hero.Body>
		</Hero>
	)
}

export default NotFoundPage;