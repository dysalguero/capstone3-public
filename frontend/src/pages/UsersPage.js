import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns,
	Container
} from 'react-bulma-components';
import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';
import Swal from 'sweetalert2';

//COMPONENTS
import UsersList from '../components/lists/UsersList';

//MUTATIONS
import {
    destroyUserMutation,
    updateUserMutation
} from '../graphql/mutations';

//QUERIES
import { 
	getUsersQuery
} from '../graphql/queries';

const UsersPage = props => {
    // console.log(props.getUsersQuery.users);

    const users = props.getUsersQuery.users;

    //view
    const viewUser = (id, username, firstName, lastName, number) => {
        Swal.fire({
            title: "View User",
            icon: "info",
            html:
                `<div class="swal2-content2">Username</div><input id="username" class="swal2-input swal2-input2" value="${username}" disabled>`+
                `<div class="swal2-content2">Name</div><input id="lastName" class="swal2-input swal2-input2" value="${firstName} ${lastName}" disabled>` +
                `<div class="swal2-content2">Number</div><input id="number" class="swal2-input swal2-input2" value="${number}" disabled>`,
			showCancelButton: false,
			reverseButtons: true,
            allowEnterKey: true,
            preConfirm: () => {
                return {
                    id: id,
                    username: username,
                    firstName: firstName,
                    lastName: lastName,
                    number: number                     
                }
            }
        })
    }

    //destroy
    const destroyUser = (id,username) => {
        Swal.fire({
            title: "Delete User",
			text: `Do you want to remove ${username}?`,
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: "Remove",
			confirmButtonColor: "#ff3860",
			reverseButtons: true
        }).then((formData) => {
            // console.log(formData)
            if(formData.value){
                props.destroyUserMutation({
                    variables: { id: id },
                    refetchQueries: [{ query: getUsersQuery }]
                })
                .then(response => {
                    // console.log(response);

                    let result = response.data.destroyUser;

                    if(result){
                        Swal.fire({
							title: "User Deleted",
							text: `${username} has been deleted.`,
							icon: "success"
						})
                    }
                })
            }
        })
    }

    //update
    const updateUser = (id, username, firstName, lastName, number, email, password, isAdmin) => {
        Swal.fire({
            title: "Update User",
			icon: "info",
			html:
                `<div class="swal2-content2">Username</div><input id="username" class="swal2-input swal2-input2" value="${username}">` + 
                `<div class="swal2-content2">First Name</div><input id="firstName" class="swal2-input swal2-input2" value="${firstName}">` +
                `<div class="swal2-content2">Last Name</div><input id="lastName" class="swal2-input swal2-input2" value="${lastName}">` +
                `<div class="swal2-content2">Number</div><input id="number" class="swal2-input swal2-input2" value="${number}">` +
                `<div class="swal2-content2">Email</div><input id="email" class="swal2-input swal2-input2" value="${email}" disabled>` +  
                `<div class="swal2-content2">Password</div><input id="password" class="swal2-input swal2-input2" value="${password}" disabled>`,
                // `<div class="swal2-content2">Admin?</div><input id="isAdmin" class="swal2-input swal2-input2" value="${isAdmin}">`,
			showCancelButton: true,
			confirmButtonText: "Update",
			confirmButtonColor: "#ffdd57",
			reverseButtons: true,
            allowEnterKey: true,
            preConfirm: () => {
                let newUsername = document.querySelector("#username").value;
                let newFirstname = document.querySelector("#firstName").value;
				let newLastname = document.querySelector("#lastName").value;
				let newNumber = document.querySelector("#number").value;
				// let newRole = document.querySelector("#isAdmin").value;

				// console.log(newName);

				if(newUsername === "" || newFirstname === "" || newLastname === "" | newNumber === "" ) {
					Swal.showValidationMessage("Please fill out all fields.");
				} else {
					return {
						id: id,
                        username: newUsername,
                        firstName: newFirstname,
                        lastName: newLastname,
                        number: newNumber,
                        email: email,
                        password: password
                        // isAdmin: newRole                        
					}
				}
			}	
        })
        .then (formData => {
            // console.log(formData);
            if(formData.value) {
                props.updateUserMutation({
                    variables: formData.value,
                    refetchQueries: [{ query: getUsersQuery }]
                })
                .then(response => {
                    console.log(response);
                })
            }
        })
    }

    return (
        <Fragment>
            <div className="sectionTitleWrapper">
				<Container className="sectionTitleHolder">
					<Heading className="fontQuentin textCapitalize size50px">Users</Heading>
				</Container>
			</div>
            <Section size="medium">
                <Container>
                    <Columns>
                        <Columns.Column>
                            <UsersList users={users} viewUser={viewUser} destroyUser={destroyUser} updateUser={updateUser}/>
                        </Columns.Column>
                    </Columns>
                </Container>
            </Section>
        </Fragment>
    )
}

export default compose(
    graphql(getUsersQuery, { name: 'getUsersQuery' }),
    graphql(destroyUserMutation, { name: 'destroyUserMutation' }),
    graphql(updateUserMutation, { name: 'updateUserMutation' }),
	)(UsersPage);