import React, {Fragment} from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Container,
	Columns
} from 'react-bulma-components';

import LoginForm from '../components/forms/LoginForm';

const LoginPage = props => {
	return (
		<Fragment>
			<div className="sectionTitleWrapper has-text-centered">
				<Container className="sectionTitleHolder">
					<Heading className="fontQuentin textCapitalize size70px">Sign in</Heading>
				</Container>
			</div>
			<Section>
				<div>
					<Columns>
						<Columns.Column></Columns.Column>
						<Columns.Column className="is-4 my-30">
							<LoginForm />
						</Columns.Column>
						<Columns.Column></Columns.Column>
					</Columns>
				</div>
			</Section>
		</Fragment>
	)
}

export default LoginPage;