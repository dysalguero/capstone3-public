import React from 'react';
// import { Link } from 'react-router-dom';

const ServiceRow = props => {

	// console.log(props);

	const service = props.service;

	return(
		<tr>
	        <td>{service.name}</td>
	        <td>{service.duration}</td>
			<td>{service.description}</td>
			<td>{service.price}.00</td>
	        <td>{service.category.name}</td>
	        <td>
				<button 
					className='button is-warning'
					onClick={() =>
						props.updateService(service.id, service.name, service.duration, service.price, service.description, service.categoryId, service.category.name)
					}>Update</button>
	          	&nbsp;
	          	<button 
				  	className='button is-danger'
				  	onClick={() => 
					  	props.destroyService(service.id, service.name)
				  	}>Remove</button>
	          	&nbsp;
	          
	          	{/* <Link to={`/service/${service.id}`}>
	          		<button className='button is-primary'>View</button>
	         	</Link> */}
	        </td>
	    </tr>
	)
}

export default ServiceRow;
