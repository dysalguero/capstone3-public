import React from 'react';
// import { Link } from 'react-router-dom';

const LashesRow = props => {

    // console.log(props.service);

    const service = props.service;

	return(
		<div className="servicesWrapper my-18">
            <div className="indivService">
                <h4>{ service.name }</h4>
            </div>
            <div className="indivServiceLine"></div>
            <div className="indivPrice">&#8369; { service.price }.00</div>
            <div className="size12px my-5">{ service.description } </div>
            {/* SHOW BUTTONS ONLY IF ADMIN */}
            {/* <button 
                className="button is-danger">
                Remove
            </button>
            &nbsp;
            <button 
                className="button is-warning">
                Update
            </button> */}
            {/* END SHOW BUTTON */}
        </div>
	)
}

export default LashesRow;
