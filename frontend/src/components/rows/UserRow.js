import React from 'react';

const UserRow = props => {
    // console.log(props.user)

    const user = props.user;

    return(
        <tr>
            <td>{user.username}</td>
            <td>{user.firstName + " " + user.lastName}</td>
            <td>{user.email}</td>
            <td>
                <button className="button is-link" onClick={() => props.viewUser(user.id, user.username, user.firstName, user.lastName, user.number)}>View</button>
                &nbsp;
				<button 
					className='button is-warning'
					onClick={() =>
						props.updateUser(user.id, user.username, user.firstName, user.lastName, user.number, user.email, user.password, user.isAdmin)
					}>Update</button>
	          	&nbsp;
	          	<button 
				  	className='button is-danger'
				  	onClick={() => 
					  	props.destroyUser(user.id, user.username)
				  	}>Remove</button>
	        </td>
        </tr>
    )
}

export default UserRow;