import React from 'react';

const CategoryRow = props => {
    // console.log(props)

    const category = props.category;

    return(
        <tr>
            <td>{category.name}</td>
            <td>
				<button 
					className='button is-warning'
					onClick={() =>
						props.updateCategory(category.id, category.name)
					}>Update</button>
	          	&nbsp;
	          	<button 
				  	className='button is-danger'
				  	onClick={() => 
					  	props.destroyCategory(category.id, category.name)
				  	}>Remove</button>
	        </td>
        </tr>
    )
}

export default CategoryRow;