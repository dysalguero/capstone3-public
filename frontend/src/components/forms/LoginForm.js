import React, {Fragment, useState} from 'react';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';
import Swal from 'sweetalert2';
import { loginMutation } from '../../graphql/mutations';

const LoginForm = props => {

    // console.log(props.currentUser().isAdmin);

    const login = e => {
		e.preventDefault();

		props.loginMutation({
			variables: {
				email, password
			}
		})
		.then(response => {
            // console.log(response.data.loginUser);

            let user = response.data.loginUser;
            if(user != null){
				//valid login credentials
				localStorage.setItem("token", user.token);

				//SWAL
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 1000,
				  timerProgressBar: true,
				  onOpen: (toast) => {
				    toast.addEventListener('mouseenter', Swal.stopTimer)
				    toast.addEventListener('mouseleave', Swal.resumeTimer)
				  }
				})

				Toast.fire({
				  icon: 'success',
				  title: 'Logged in successfully'
				}).then(()=>{
                    return window.location = "/";
				})

			} else {
				//invalid login credentials
				Swal.fire({
					title: "Login Failed",
					text: "Please enter valid credentials",
					icon: "error"
				})
			} 
		})
	}
        
    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    
    return (
        <Fragment>
            <form onSubmit={e => login(e)}>  
                <div className="field">
                    <label className='label has-text-weight-normal'>Email</label>
                    <p className="control has-icons-left has-icons-right">
                        
                        <input 
                            className="input" 
                            type="email"  
                            value={email}
                            onChange={e => setEmail(e.target.value)} />
                        <span className="icon is-small is-left">
                            <i className="fas fa-envelope"></i>
                        </span>
                    </p>
                </div>
                <div className="field">
                    <label className='label has-text-weight-normal'>Password</label>
                    <p className="control has-icons-left">
                        <input 
                            className="input" 
                            type="password" 
                            value={password}
                            onChange={e => setPassword(e.target.value)}/>
                        <span className="icon is-small is-left">
                            <i className="fas fa-lock"></i>
                        </span>
                    </p>
                </div>
                <div className="field">
                    <p className="control">
                        <button className="button is-dark">
                            Login
                        </button>
                    </p>
                </div>
            </form>
        </Fragment>
    )
}

export default compose(
	graphql(loginMutation, { name: 'loginMutation' })
)(LoginForm);