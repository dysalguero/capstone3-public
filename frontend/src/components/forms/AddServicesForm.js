import React, { Fragment, useState } from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card,
	Button
} from 'react-bulma-components';

import { graphql } from 'react-apollo';
import { getCategoriesQuery } from '../../graphql/queries';


const AddServicesForm = (props) => {

    // console.log(props);

    const [ name, setName ] = useState("");
    const [ duration, setDuration ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ price, setPrice ] = useState("");
    const [ categoryId , setCategoryId ] = useState(undefined);

    //function addServices
    const addService = (e) => {
        e.preventDefault();

        const newService = {
            name,
            duration,
            price,
            description,
            categoryId
        }

        setName("");
        setDuration("");
        setDescription("");
        setPrice("");
        setCategoryId("DEFAULT");

        props.addService(newService);

        Swal.fire({
            title: "Service Added",
            text: `${name} has been added!`,
            icon: "success"
        })
    }

    //function populateCategoryOption
    const populateCategoryOption = () => {
        // console.log(props.data);

        let data = props.data;

        if(data.loading) {
            return <option>Loading categories...</option>
        } else {
            return data.categories.map(category => {
                return (
                    <option key={category.id} value={category.id}>{category.name}</option>
                )
            })
        }
    }

    return (
        <Fragment>
            <Card>
                <Card.Header>
                    <Card.Header.Title>Add Services</Card.Header.Title>
                </Card.Header>
                <Card.Content>
                    <form onSubmit={addService}>
                        <div className="field">
                            <label className="label">Name</label>
                            <div className="control">
                                <input 
                                className="input" 
                                type="text" 
                                required
                                value={name}
                                onChange={ e => setName(e.target.value) }/>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Duration</label>
                            <div className="control">
                                <input 
                                className="input" 
                                type="text" 
                                required
                                value={duration}
                                onChange={ e => setDuration(e.target.value) }/>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Description</label>
                            <div className="control">
                                <input 
                                className="input" 
                                type="text" 
                                required
                                value={description}
                                onChange={ e => setDescription(e.target.value) }/>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Price</label>
                            <div className="control">
                                <input 
                                className="input" 
                                type="text" 
                                required
                                value={price}
                                onChange={ e => setPrice(e.target.value) }/>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Category</label>
                            <div className="control">
                                <div className="select is-fullwidth">
                                    <select 
                                        defaultValue={"DEFAULT"}
                                        required
                                        value={categoryId}
                                        onChange={ e => setCategoryId(e.target.value) }
                                    >
                                        <option value="DEFAULT" disabled>Select Category</option>
                                        { populateCategoryOption() }
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="field">
						<div className="control">
							<Button type="submit" className="button is-link is-success">
								Add
							</Button>
						</div>
					</div>
                    </form>
                </Card.Content>
            </Card>
        </Fragment>
        
    )
}
export default graphql(getCategoriesQuery)(AddServicesForm);