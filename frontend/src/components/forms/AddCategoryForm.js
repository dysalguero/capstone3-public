import React, { Fragment, useState } from 'react';
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card,
	Button
} from 'react-bulma-components';

import { graphql } from 'react-apollo';
import { getCategoriesQuery } from '../../graphql/queries';

const AddCategoryForm = props => {
    const [ name, setName ] = useState("");

    // function Store
    const addCategory = (e) => {
        e.preventDefault();

        const newCategory = {
            name
        }

        setName("");

        props.addCategory(newCategory);

        Swal.fire({
            title: "Category Added",
            text: `${name} has been added!`,
            icon: "success"
        })
    }

    return (
        <Fragment>
            <Card>
                <Card.Header>
                    <Card.Header.Title>Add Category</Card.Header.Title>
                </Card.Header>
                <Card.Content>
                    <form onSubmit={addCategory}>
                        <div className="field">
                            <label className="label">Name</label>
                            <div className="control">
                                <input 
                                className="input" 
                                type="text" 
                                required
                                value={name}
                                onChange={ e => setName(e.target.value) }/>
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <Button type="submit" className="button is-link is-success">
                                    Add
                                </Button>
                            </div>
                        </div>
                    </form>
                </Card.Content>
            </Card>
        </Fragment>
    )
}

export default graphql(getCategoriesQuery)(AddCategoryForm);