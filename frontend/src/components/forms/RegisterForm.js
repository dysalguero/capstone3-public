import React, {Fragment, useState} from 'react';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';

import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';
import { registerUserMutation } from '../../graphql/mutations'

const RegisterForm = props => {
    // console.log(useState("hello"));

    // console.log(props);

    const [ firstName, setFirstName ] = useState("");
    const [ lastName, setLastName ] = useState("");
    const [ number, setNumber ] = useState("");
    const [ username, setUsername ] = useState("");
	const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ isDisabled, setIsDisabled ] = useState(true);
    const [ goToLogin, setGoToLogin ] = useState(false);

    //check if password is greater than or equal to 8
	const checkPassword = (password) => {
		//call function and assign props
		setPassword(password);

		//conditional rendering
		if(password.length >= 8){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
    }


    //register
	const register = e => {
		e.preventDefault();
		props.registerUserMutation({
			variables: {
				firstName, lastName, number, username, email, password
			}
		})
		.then(response => {
			// console.log(response);
			const newUser = response.data.registerUser;

			if(newUser){
				Swal.fire({
					title: "Registration Successful",
					text: "You will be redirected to login page",
					icon: "success"
				})
				.then(()=>{
					setGoToLogin(true)
				})
			} else {
				Swal.fire({
					title: "Registration Failed",
					text: "The server encountered an error",
					icon: "error"
				})
			}
		})
	}
    
    //redirect login once registered
	if(goToLogin){
		return <Redirect to="/login?register=true" />
	}

    return (
        <Fragment>
            <form onSubmit={e => register(e)}>  
                <div className="field">
                    <label className="label has-text-weight-normal">First Name</label>
                    <div className="control">
                        <input 
                            className="input" 
                            type="text"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)} />
                    </div>
                </div>
                <div className="field">
                    <label className='label has-text-weight-normal'>Last Name</label>
                    <p className="control">
                        <input 
                            className="input" 
                            type="text"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)} />
                    </p>
                </div>
                <div className="field">
                    <label className='label has-text-weight-normal'>Contact Number</label>
                    <p className="control">
                        <input 
                            className="input" 
                            type="number" 
                            value={number}
                            onChange={e => setNumber(e.target.value)}
                            />
                    </p>
                </div>               
                <div className="field">
                    <label className='label has-text-weight-normal'>Username</label>
                    <p className="control">
                        <input 
                            className="input" 
                            type="text" 
                            value={username}
                            onChange={e => setUsername(e.target.value)}
                            />
                    </p>
                </div>
                <div className="field">
                    <label className='label has-text-weight-normal'>Email</label>
                    <p className="control">
                        <input 
                            className="input" 
                            type="email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            />
                    </p>
                </div>
                <div className="field">
                    <label className='label has-text-weight-normal'>Password</label>
                    <p className="control">
                        <input 
                            className="input" 
                            type="password" 
                            value={password}
                            onChange={e => checkPassword(e.target.value)}
                            />
                    </p>
                </div>
                <div className="field">
                    <p className="control">
                        <button 
                            type='submit'
                            className="button is-dark is-link"
                            disabled={ isDisabled }>
                            Register
                        </button>
                    </p>
                </div>
            </form>
        </Fragment>
    )
}

export default compose(
	graphql(registerUserMutation, { name: 'registerUserMutation' })
)(RegisterForm);