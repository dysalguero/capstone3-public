import React, { useState } from 'react';
// import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Button,
    Columns
} from 'react-bulma-components';
import compose from 'lodash.flowright';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import { graphql } from 'react-apollo';
import {
    getCategoriesQuery,
    getservicesByCategory
} from '../../graphql/queries';

//component
import TimeslotList from '../lists/TimeslotList';

const AddAppointmentForm = (props) => {

    // const [ userId, setUserId ] = useState("");
    const [ categoryId, setCategoryId ] = useState(undefined);
    const [ serviceId, setServiceId ] = useState("");
    const [ bookingDate, setBookingDate ] = useState(undefined);
    const [ timeslotId, setTimeslotId ] = useState("");

    //function addAppointment
    const addAppointment = (e) => {
        e.preventDefault();

        const newAppointment = {
            // userId,
            categoryId,
            serviceId,
            bookingDate,
            // timeslot
        }

        props.addAppointment(newAppointment);
    }

    //timeslots
    let timeslotRow = "";

    //populate timeslot
    if(typeof props.timeslots === "undefined" || props.timeslots.length === 0 ) {
        timeslotRow = (
            <em>No timeslot found.</em>
        )
    } else {
        timeslotRow = props.timeslots.map(timeslot => {
            return (
                <TimeslotList timeslot={timeslot} key={timeslot.id} value={timeslot.id} />
            )
        })
    }

    const handleChooseTimeslot = id => {
        setTimeslotId(id);
    }

    //populate Category options
    const populateCategoryOption = () => {
        let data = props.getCategoriesQuery;

        if(data.loading) {
            return <option>Loading categories...</option>
        } else {
            return data.categories.map(category => {
                return (
                    <option key={category.id} value={category.id}>{category.name}</option>
                )
            })
        }
    }

    // let categorySelected = categoryId;
    // let data = props.getservicesByCategory;
    // console.log(categoryId)

    // //pass services based on category selected
    // const populateServicesByCategory = (categorySelected) => {
    //     let options = {};
    //     props.data.services.map(service => {
    //         return service.category.name === props.categorySelected ?
    //         options[service.id] = service.name : ""
    //     })
    //     return options;
    // }

    //disable timeslot when already booked

    // console.log(categoryId)
    // console.log(bookingDate)
    // console.log(timeslotId)
    const today = new Date();

    return (
        <form>
            <h1 className="my-30">Please select a service:</h1>
            <Columns>
                {/* Category */}
                <Columns.Column>
                    <label className="label">Category</label>
                    <div className="control is-full">
                        <div className="select is-fullwidth">
                            <select 
                                defaultValue={"DEFAULT"}
                                required
                                value={categoryId}
                                onChange={ e => setCategoryId(e.target.value) }>
                                <option value="DEFAULT" disabled>Select Category</option>
                                { populateCategoryOption() }
                            </select>
                        </div>
                    </div>
                </Columns.Column>
                {/* Service */}
                <Columns.Column>
                    <label className="label">Service</label>
                    <div className="control is-full">
                        <div className="select is-fullwidth">
                            <select>
                                <option>Select dropdown</option>
                                <option>With options</option>
                                {/* {populateServicesByCategory()} */}
                            </select>
                        </div>
                    </div>
                </Columns.Column>
                {/* Date */}
                <Columns.Column>
                    <label className="label">Date</label>
                    <DayPicker 
                        disabledDays={{ before: today, daysOfWeek: [0] }}
                        value={bookingDate} 
                        onDayClick={e => {
                            setBookingDate(e);
                            console.log(bookingDate);
                        }}/>
                        {bookingDate ? (<p className="has-text-centered">You clicked <b>{bookingDate.toLocaleDateString()}</b></p>
                            ) : (
                            <p className="has-text-centered">Please select a day.</p>
                            )}
                </Columns.Column>
                {/* Timeslot */}
                <Columns.Column>
                    <label className="label">Time Slot</label>
                    { 
                        //function to populate timeslot
                        (typeof props.timeslots === "undefined" || props.timeslots.length === 0 ) ?
                         <em>No timeslot found.</em> :
                         props.timeslots.map(timeslot => {
                            return (
                                <a className="button is-link my-5 mr-5 flex-box" timeslot={props.timeslot} key={timeslot.id} value={timeslot.id} onClick={ () => handleChooseTimeslot(timeslot.id)}>{timeslot.time}</a>
                            )
                        })
                    }
                </Columns.Column>
            </Columns>

            <div className="field">
                <div className="control">
                    <Button type="submit" className="button is-link is-success">
                        Add Appointment
                    </Button>
                </div>
            </div>
        </form>
    )
}

export default compose(
    graphql(getCategoriesQuery, { name: 'getCategoriesQuery' }),
    graphql(getservicesByCategory, { name: "getservicesByCategory" }),
    )(AddAppointmentForm);
    
    