import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';

import CategoryRow from '../rows/CategoryRow';

const CategoriesList = props => {
    // console.log(props)
    let categoryRow = "";

    if(typeof props.categories === "undefined" || props.categories.length === 0 ) {
        categoryRow = (
            <tr>
                <td colSpan="2">
                <em>No categories found.</em>
                </td>
            </tr>
        )
    } else {
        categoryRow = props.categories.map(category => {
        return <CategoryRow category={category} key={category.id} destroyCategory={props.destroyCategory} updateCategory={props.updateCategory}/>
        })
    }

    return (
        <Card>
            <Card.Header>
                <Card.Header.Title>Categories List</Card.Header.Title> 
            </Card.Header>
            <Card.Content>
                <table className='table is-fullwidth is-bordered is-striped is-hoverable'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    { categoryRow }
                </tbody>
                </table>
            </Card.Content>
        </Card>
    )
}

export default CategoriesList;