import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Container,
    Columns
} from 'react-bulma-components';

import FaceRow from '../rows/FaceRow';

const FaceList = props => {
    // console.log(props);

    let serviceRow = "";

    if(typeof props.services === "undefined" || props.services.length === 0) {
        serviceRow = (
            <em>No services found.</em>
        )
    } 
    else {
        serviceRow = props.services.map(service => {
             return service.category.name === props.categoryName ?
             <FaceRow service={service} key={service.id} /> : ""
        })
    }

    return (
        <Container>
            <Columns>
                <Columns.Column>
                    <h1 className="size40px is-pulled-right my-18">Face</h1>
                    <div className="backTextServices is-hidden-touch">
						<div className="fontQuentin textCapitalized">Rejuvenate</div>
					</div>
                </Columns.Column>
                <br /><br /><br />
                <Columns.Column>
                    { serviceRow }
                </Columns.Column>
            </Columns>
        </Container>	  
    )
}

export default FaceList;