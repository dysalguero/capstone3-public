import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';

import ServiceRow from '../rows/ServiceRow';

const ServicesList = props => {

    // console.log(props)

    let serviceRow = "";

    if(typeof props.services === "undefined" || props.services.length === 0 ) {
        serviceRow = (
            <tr>
                <td colSpan="4">
                <em>No services found.</em>
                </td>
            </tr>
        )
    } else {
        serviceRow = props.services.map(service => {
        return <ServiceRow service={service} key={service.id} destroyService={props.destroyService} updateService={props.updateService}/>
        })
    }

    return (
        <Card>
            <Card.Header>
                <Card.Header.Title>Service List</Card.Header.Title>
            </Card.Header>
            <Card.Content>
                <table className='table is-fullwidth is-bordered is-striped is-hoverable'>
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Duration</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    { serviceRow }
                </tbody>
                </table>
            </Card.Content>
        </Card>
    )

}

export default ServicesList;