import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Container,
    Columns
} from 'react-bulma-components';

import LashesRow from '../rows/LashesRow';

const LashesList = props => {
    // console.log(props)

    let serviceRow = "";

    if(typeof props.services === "undefined" || props.services.length === 0) {
        serviceRow = (
            <em>No services found.</em>
        )
    } 
    else {
        serviceRow = props.services.map(service => {
            return service.category.name === props.categoryName ?
             <LashesRow service={service} key={service.id} /> : ""
        })
    }
    
    return (
        <Columns.Column>
            <h1 className="size40px has-text-centered">Lashes</h1>
            <Container className="my-30">
                {/* INDIV SERVICES */}
                { serviceRow }
            </Container>
        </Columns.Column>
    )
}

export default LashesList;