import React, { Fragment } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Columns
} from 'react-bulma-components';

import BrowsRow from '../rows/BrowsRow';

const BrowsList = props => {

    let serviceRow = "";

    if(typeof props.services === "undefined" || props.services.length === 0) {
        serviceRow = (
            <em>No services found.</em>
        )
    } 
    else {
        serviceRow = props.services.map(service => {
            return service.category.name === props.categoryName ?
             <BrowsRow service={service} key={service.id} /> : ""
        })
    }

    return (
        <Fragment>
            <Columns.Column>
                <h1 className="size40px is-pulled-right">Brows</h1>
                <div className="backTextServices2 is-hidden-touch">
                            <div className="fontQuentin textCapitalized">Beautify</div>
                        </div>
                <br/><br/>
            </Columns.Column>
            <Columns.Column>
                {/* INDIV SERVICES */}
                { serviceRow }
            </Columns.Column>
        </Fragment>
        
    )
}

export default BrowsList;
