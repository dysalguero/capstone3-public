import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
    Container,
    Button
} from 'react-bulma-components';

// import TimeslotRow from '../rows/TimeslotRow';

const TimeslotList = props => {
    // console.log(props)

    const timeslot = props.timeslot;

    return (
        <Container>
            <a className="button is-link my-5" value={timeslot.id}>{timeslot.time}</a>
        </Container>
    )
}

export default TimeslotList;