import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
Card
} from 'react-bulma-components';

import UserRow from '../rows/UserRow';

const UsersList = props => {

    let userRow = "";

    if(typeof props.users === "undefined" || props.users.length === 0 ) {
        userRow = (
            <tr>
                <td colSpan="5">
                <em>No users found.</em>
                </td>
            </tr>
        )
    } else {
        userRow = props.users.map(user => {
        return <UserRow user={user} key={user.id} viewUser={props.viewUser} destroyUser={props.destroyUser} updateUser={props.updateUser}/>
        })
    }

    return (
        <Card>
            <Card.Header>
                <Card.Header.Title>Users List</Card.Header.Title> 
            </Card.Header>
            <Card.Content>
                <table className='table is-fullwidth is-bordered is-striped is-hoverable'>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    { userRow }
                </tbody>
                </table>
            </Card.Content>
        </Card>
    )
}

export default UsersList;