import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Navbar, Container } from 'react-bulma-components';
import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';

import { 
	getCategoriesQuery
} from '../graphql/queries';

const AppNavbar = props => {   

    // const categories = props.getCategoriesQuery.categories;

    // console.log(props)

    //Populate Categories
    // let navBarOptions = "";
    // if(typeof props.getCategoriesQuery.categories === "undefined" || props.getCategoriesQuery.categories.length === 0 ) {
    //     navBarOptions = (
    //         <Link className="navbar-item" to="/services">
    //             Loading...
    //         </Link>
    //     )
    //   } else {
    //     navBarOptions = props.getCategoriesQuery.categories.map(category => {
    //       return (
    //             <Link className="navbar-item" to={`/services/${category.name}`} key={category.id} value={category.id}>
    //                 {category.name}
    //             </Link>
    //        )
    //     })
    //   }

    //Display different navlinks    
    let endNav = "";
    let startNav = "";

    if(props.isAdmin === null || props.isAdmin === false){
        startNav = (
	        <Fragment>
                <Link className="navbar-item" to="/">
                    Home
                </Link>
                <Link className="navbar-item" to="/about">
                    About
                </Link>
                <Link className="navbar-item" to="/services">
                    Services
                </Link>
                <Link className="navbar-item" to="/appointment">
                    Book Now
                </Link>
	        </Fragment>
		)
    } 

    if(props.token){
        endNav = (
            <Fragment>
                <Link className="navbar-item" to="/">
                    Hi, {props.firstName}!
                </Link>
                <Link className="navbar-item" to="/logout">
                    Log out
                </Link>
            </Fragment>
        )
    } else {
        endNav = (
            <Fragment>
                <Link className="navbar-item" to="/login">
                    Log in
                </Link>
                <Link className="navbar-item" to="/register">
                    Register
                </Link>
            </Fragment>
        )
    }
    
    if(props.isAdmin) {
        startNav = (
        <div className="navbar-item has-dropdown is-hoverable">
            <Link className="navbar-link" to="/">
                Dashboard
                </Link>
                    <div className="navbar-dropdown is-boxed">
                        <Link className="navbar-item" to="/services">
                            Services
                        </Link>
                        <Link className="navbar-item" to="/categories">
                            Categories
                        </Link>
                        <Link className="navbar-item" to="/appointments">
                            Appointments
                        </Link>
                        <Link className="navbar-item" to="/users">
                            Users
                        </Link>
                    </div>
                
            </div>
        )
    }

    return (
        <Navbar className="is-white is-uppercase is-fixed-top">
            <Container>
                <Navbar.Brand>
                    <a className="navbar-item" href="/">
                        <img src="../images/logo-black.png" alt="logo" className="logoFooterImg" />
                    </a>
                    <Navbar.Burger className="burger" data-target="navbarBurger" />
                </Navbar.Brand>
                <Navbar.Menu id="navbarBurger">
                    <Navbar.Container>
                        
                        {/* <div className="navbar-item has-dropdown is-hoverable">
                            
                            <div className="navbar-dropdown is-boxed">
                                { navBarOptions }
                            </div>
                        </div> */}
                        {/* SHOW ONLY FOR USER */}
                        
                        { startNav }                         
                    </Navbar.Container>
                    <Navbar.Container position="end">
                        <div className="field is-grouped">
                            { endNav }                            
                        </div>
                    </Navbar.Container>
                </Navbar.Menu>
            </Container>
        </Navbar>
    )
}

export default compose(
	graphql(getCategoriesQuery, { name: 'getCategoriesQuery' })
	)(AppNavbar);