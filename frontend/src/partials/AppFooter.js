import React, { Fragment } from 'react';
// import { Link } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Container, Section, Columns } from 'react-bulma-components';

const AppFooter = props => {

    return (
        <Fragment>
            <div className="footerTop">
                <Section>
                    <Container>
                        <Columns>
                        <Columns.Column className="has-text-centered">                                {/* LOGO */}
                                <div className="has-text-centered fontQuentin footerTitle my-5">
                                    <img src="../images/logo-white.png" alt="the lazy babe logo" className="logoFooterImg" />
                                </div>
                                <div className="my-5 footerText">Welcome to one of the most relaxing salons, where we offer tranquility whilst being treated. Visit us for free consultation now.</div>
                                <br/>
                                <div className="has-text-centered">
                                    <div className="footerSocMedLink">
                                        <a href="http://www.facebook.com/"><i className="fab fa-facebook-f footerSocMedFirst"></i></a>
                                        <a href="http://www.instagram.com/"><i className="fab fa-instagram footerSocMed"></i></a>
                                        <a href="https://gitlab.com/dysalguero"><i className="fab fa-gitlab footerSocMed"></i></a>
                                        <a href="http://www.linkedin.com/"><i className="fab fa-linkedin-in footerSocMedLast"></i></a>
                                    </div>
                                </div>
                            </Columns.Column>
                            <Columns.Column className="has-text-centered">                                {/* CONTACT */}
                                <div className="has-text-centered fontQuentin footerTitle my-5">Contact</div>
                                
                                <div className="footerText my-5">
                                <br/>
                                    <i className="fas fa-map-marker-alt my-5"></i> 5th Ave, Taguig, Metro Manila 1634
                                    <br/>
                                    <i className="fas fa-envelope-open-text my-5"></i>    info@example.com
                                    <br />
                                    <i className="fas fa-phone my-5"></i>    +02 8876 1234
                                </div>
                            </Columns.Column>
                            <Columns.Column className="has-text-centered">
                                {/* BUSINESS HOURS */}
                                <div className="has-text-centered fontQuentin footerTitle my-5">Business Hours</div>
                                <Columns.Column className="footerText">
                                    <div className="my-5">Mondays-Fridays: 11AM - 8PM</div>
                                    <div className="my-5">Saturdays: 11AM - 9PM</div>
                                    <div className="my-5">Sundays: Closed</div>
                                </Columns.Column>
                            </Columns.Column>
                            <Columns.Column className="verticalAlign has-text-centered">
                                {/* MAPS */}
                                <div className="has-text-centered fontQuentin footerTitle my-5">Map</div>
                                <br/>
                                <img src="../images/footerMap.png" alt="map" width="500px"/>
                            </Columns.Column>
                        </Columns>
                    </Container>
                </Section>
            </div>
            <div className="has-text-centered footerBottom">
                <span>Copyright 2019 &copy; thelazybabe by 
                    <span className="f-sign">
                        <a href="https://gitlab.com/dysalguero" target="_blank" rel="noopener noreferrer"> Sandy</a>
                    </span>
                </span>
            </div>
        </Fragment>
        
    )
}

export default AppFooter;