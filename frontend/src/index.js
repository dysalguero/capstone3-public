import React from 'react';
import ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import App from './app';
import './index.css';
import './animate.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './fonts/Quentin.otf';

//apollo client
const client = new ApolloClient({ uri: 'http://localhost:4000/graphql' })

const root = document.querySelector("#root");
const pageComponent = (
	<ApolloProvider client={client}>
		<App />
	</ApolloProvider>
);

ReactDOM.render(pageComponent, root);