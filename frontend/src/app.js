import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import jwt from 'jsonwebtoken';
import verifyToken from './jwt-verify';

//partials
import AppNavbar from './partials/AppNavbar';
import AppFooter from './partials/AppFooter';

//pages
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import AppointmentPage from './pages/AppointmentPage';
import ServicesPage from './pages/ServicesPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import FacePage from './pages/FacePage';
import NotFoundPage from './pages/NotFoundPage';

//admin pages
import CategoriesPage from './pages/CategoriesPage';
// import AppointmentsPage from './pages/AppointmentsPage';
import UsersPage from './pages/UsersPage';



//auth route
// const AuthRoute = ({token, ...props}) => {
// 	return token ? <Route {...props} /> : <Redirect to="/login" />
// }

// admin route
// const AdminRoute = ({token, isAdmin, ...props}) => {
// 	if(token && isAdmin){
// 		//if token and admin
// 		return <AuthRoute {...props} token={token} />
// 	} else if(token && !isAdmin){
// 		//if token but user
// 		return <Redirect to="/" />
// 	} else {
// 		//if wala lahat
// 		return <Redirect to="/login" />
// 	}
// }

const App = () => {

    const [ token, setToken ] = useState(localStorage.getItem("token"));

    const decoded = verifyToken(token);
    // console.log(decoded);

    const [ isAdmin, setIsAdmin] = useState(decoded ? decoded.isAdmin : null);
    const [ username, setUsername ] = useState(decoded ? decoded.username : null);
    const [ firstName, setFirstname ] = useState(decoded ? decoded.username : null);
    
    //update sesssion in login and logout
	const updateSession = () => {
		setToken(token);
		setIsAdmin(decoded.isAdmin);
        setUsername(decoded.username);
        setFirstname(decoded.firstName);
    }
    
    const Logout = (props) => {
		localStorage.clear();
		updateSession();
		window.location = "/login";
	}

    const currentUser = () => {
		return { username, isAdmin, token }
    }

    //set props for each page
    const Login = (props) => <LoginPage {...props} token={token} updateSession={updateSession} />
    const Home = (props) => <HomePage {...props} currentUser={currentUser} updateSession={updateSession} />
    const Services = (props) => <ServicesPage {...props} currentUser={currentUser} updateSession={updateSession} />
    const Categories = (props) => <CategoriesPage {...props} currentUser={currentUser} updateSession={updateSession} />
    const Appointment = (props) => <AppointmentPage {...props} currentUser={currentUser} updateSession={updateSession} />
    const Users = (props) => <UsersPage {...props} currentUser={currentUser} updateSession={updateSession} />


    return (
        <BrowserRouter>
            <AppNavbar username={username} firstName={firstName} isAdmin={isAdmin} token={token} />
            <Switch>
                <Route component={Home} exact path="/" />
                <Route component={AboutPage} exact path="/about" />
                <Route component={Services} exact path="/services" />
                <Route component={Categories} exact path="/categories" />
                {/* <Route component={Appointments} exact path="/appointments" /> */}
                <Route component={Users} exact path="/users" />
                <Route component={Appointment} exact path="/appointment" />
                <Route component={FacePage} exact path="/services/face" />
                <Route component={RegisterPage} exact path="/register" />
		    	<Route render={Login} path="/login" />
                <Route render={Logout} path="/logout" />
                <Route component={NotFoundPage} />
            </Switch>
            <AppFooter username={username} firstName={firstName} isAdmin={isAdmin} token={token} />
        </BrowserRouter>
    )
}

export default App;