import {gql} from 'apollo-boost';

const getCategoriesQuery = gql`
    {
        categories {
            id
            name
            services {
                id
                name
                description
                duration
                price
            }
        }
    }
`;

const getServicesQuery = gql`
    {
        services {
            id
            name
            duration
            description
            price
            categoryId
            category {
                name
            }
        }
    }
`;

const getservicesByCategory = gql`
    query(
        $id: ID!
    ){
        servicesByCategory(
            id: $id
        ) {
            id
            name
            price
            description
            duration
        }
    }
`;

const getservicesByCategoryName = gql`
    query(
        $name: String!
    ){
        servicesByCategoryName(
            name: $name
        ) {
            id
            name
            price
            description
            duration
        }
    }

`;

const getAppointmentsQuery = gql`
    {
        appointments {
            id
            userId
            categoryId
            category {
                name
            }
            serviceId
            service {
                name
            }
            bookingDate
            timeslotId
            timeslot {
                time
            }
            isCompleted
            dateCompleted
        }
    }
`;

const getUsersQuery = gql `
    {
        users {
            id
            username
            firstName
            lastName
            number
            email
            isAdmin
            appointments {
                id
            }
        }
    }
`;

const getTimeslotQuery = gql`
    {
        timeslots {
            id
            time
        }
    }
`;

export { getCategoriesQuery, getServicesQuery, getservicesByCategory, getservicesByCategoryName, getAppointmentsQuery, getUsersQuery, getTimeslotQuery };