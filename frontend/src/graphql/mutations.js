import { gql } from 'apollo-boost';

const registerUserMutation = gql `
    mutation(
        $firstName: String!
        $lastName: String!
        $number: String!
        $username: String!
        $email: String!
        $password: String!
    ) {
        registerUser(
            firstName: $firstName
            lastName: $lastName
            number: $number
            username: $username
            email: $email
            password: $password
        )
    }
`;

const loginMutation = gql `
    mutation(
        $email: String!
        $password: String!
    ) {
        loginUser(
            email: $email
            password: $password
        ) {
            username
            isAdmin
            token
        }
    }
`;

// ================================ STORE

const storeServiceMutation = gql `
    mutation(
        $name: String!
        $duration: String!
        $price: String!
        $description: String!
        $categoryId: String!
    ) {
        storeService(
            name: $name
            duration: $duration
            price: $price
            description: $description
            categoryId: $categoryId
        ) {
            id
            name
            duration
            price
            description
            categoryId
        }
    }
`;

const StoreCategoryMutation = gql `
    mutation(
        $name: String!
    ) {
        storeCategory(
            name: $name
        ) {
            id
            name
        }
    }
`;

// ================================ DELETE

const destroyServiceMutation = gql `
    mutation(
        $id: ID!
    ) {
        destroyService(
            id: $id
        ) {
            id
            name
        }
    }
`;

const destroyCategoryMutation = gql `
    mutation(
        $id: ID!
    ) {
        destroyCategory(
            id: $id
        ) {
            id
            name
        }
    }
`;

const destroyUserMutation = gql `
    mutation(
        $id: ID!
    ) {
        destroyUser(
            id: $id
        ) {
            id
            username
        }
    }
`;

// ================================ UPDATE

const updateServiceMutation = gql `
    mutation(
        $id: ID!
        $name: String!
        $duration: String!
        $price: String!
        $description: String!
        $categoryId: String!
    ) {
        updateService(
            id: $id
            name: $name
            duration: $duration
            price: $price
            description: $description
            categoryId: $categoryId
        ) {
            id
            name
            duration
            price
            description
            categoryId
        }
    }
`;

const updateCategoryMutation = gql `
    mutation(
        $id: ID!
        $name: String!
    ) {
        updateCategory(
            id: $id
            name: $name
        ) {
            id
            name
        }
    }
`;

const updateUserMutation = gql `
    mutation(
        $id: ID!
        $username: String!
        $firstName: String!
        $lastName: String!
        $number: String!
    ) {
        updateUser(
            id: $id
            username: $username
            firstName: $firstName
            lastName: $lastName
            number: $number
        ) {
            id
            username
            firstName
            lastName
            number
        }
    }
`;


export { registerUserMutation, loginMutation, storeServiceMutation, destroyServiceMutation, updateServiceMutation, StoreCategoryMutation, destroyCategoryMutation, updateCategoryMutation, destroyUserMutation,updateUserMutation };